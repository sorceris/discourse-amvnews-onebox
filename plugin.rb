# name: Discourse AMVNews.ru Onebox
# about: Adds support for properly embedding AMVNews.ru videos within Discourse.
# version: 1.0
# authors: Sojiro

class Onebox::Engine::AMVNewsOnebox
	include Onebox::Engine

	REGEX = /^https?:\/\/(?:www\.)?amvnews\.ru\/index.php\?go=Files&in=view&id=(\d+)$/
	matches_regexp REGEX

	def id
		@url.match(REGEX)[1]
	end
	
	def to_html
		"<iframe width=\"480\" height=\"318\" src=\"//amvnews.ru/index.php?go=Files&file=embed&id=#{id}\"  frameborder=\"0\" allowfullscreen></iframe>"
	end
end

